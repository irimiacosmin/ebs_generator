import models.Publication;
import models.Subscription;
import utils.FileUtil;
import generators.PublicationGenerator;
import generators.SubscriptionGenerator;

import java.util.List;

import static utils.FileUtil.saveAndSerialize;

public class Main {

    public static void main(String[] args) {
        List<Publication> publications = initAndGeneratePublications();
        List<Subscription> subscriptions = initAndGenerateSubscriptions();
        publications.forEach(System.out::print);
        subscriptions.forEach(System.out::print);
        saveAndSerialize(publications, subscriptions, false);
    }

    private static List<Publication> initAndGeneratePublications() {
        PublicationGenerator publicationGenerator = (PublicationGenerator) FileUtil.constructGenerator("publication");
        return publicationGenerator.generate();
    }

    private static List<Subscription> initAndGenerateSubscriptions() {
        SubscriptionGenerator subscriptionGenerator = (SubscriptionGenerator) FileUtil.constructGenerator("subscription");
        return subscriptionGenerator.generate();
    }
}
