# README #

### Short description ###

* The Generator will read all the frequencies and limits from input.json.
* TOTAL_MESSAGES empty Publications and Subscriptions will be created.
* For every rule, the Generator will compute the min and max for the applicable types taking into consideration the limits if applicable.
* After this the Generator will start applying the rules: 
	1. for NAME we can apply = (insert the exact value that use want) or != (generate a random company name from a predefined list without using the user value).
	2. for everything else we can apply <, >, <=, >=. Every operation will compute the interval for a valid Double or Date random value.
* The project also has options for saving the Publications and Subscriptions with the possibility of serialization/deserialization.